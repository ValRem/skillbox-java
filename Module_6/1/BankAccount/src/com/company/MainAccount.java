package com.company;

public class MainAccount {
    private double asset;

    public void refillAccount(double sumOfRefill) {
        asset = getAsset() + sumOfRefill;
        System.out.println("Your current asset refilled by: " + sumOfRefill + " and now equals: " + asset);
    }

    public void withdrawSum(double sumToWithdraw) {
        if (sumToWithdraw <= getAsset()) {
            asset = getAsset() - sumToWithdraw;
            System.out.println("Your current asset reduced by : " + sumToWithdraw + " and now equals: " + getAsset());
        } else {
            System.out.println("This sum can not be withdrawn due to shortage of asset.");
        }
    }

    public double getAsset() {
        return this.asset;
    }
}
