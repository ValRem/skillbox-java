package com.company;

public class Deposit extends MainAccount {
    private static final int FIXED_NO_TAKE_TIME = 30;
    long timeOfLastPut;

    public long getTimeOfLastPut() {
        return timeOfLastPut;
    }

    public void setTimeOfLastPut(long timeOfLastPut) {
        this.timeOfLastPut = timeOfLastPut;
    }

    public static int getFixedNoTakeTime() {
        return FIXED_NO_TAKE_TIME;
    }
}
