package com.company;

import java.util.Scanner;
import java.time.Duration;

public class Main {

    public static void main(String[] args) {
        MainAccount mainAccount = new MainAccount();
        Deposit deposit = new Deposit();
        CardAccount cardAccount = new CardAccount();

        while (true) {
            System.out.println("Type an account (Main or Deposit or Card) and  operation you wish to fulfill" +
                    " (SHOW or TAKE or PUT)");
            Scanner scanner = new Scanner(System.in);
            String orderString = scanner.nextLine();
            String[] orderArray = orderString.split("\\s+");
            String accountToOperate = orderArray[0];
            if (orderArray.length < 2) {
                System.out.println("Your input is invalid");
            } else {
                String toDoString = orderArray[1];
                if (accountToOperate.equals("Main")) {
                    switch (toDoString) {
                        case ("SHOW"): {
                            System.out.println("Current asset at Main Account: " + mainAccount.getAsset());
                        }
                        break;
                        case ("PUT"): {
                            System.out.println("Type sum to put");
                            String sumText = scanner.next();
                            double sumToPut = Double.parseDouble(sumText);
                            mainAccount.refillAccount(sumToPut);
                        }
                        break;
                        case ("TAKE"): {
                            System.out.println("Type sum to take");
                            String sumText = scanner.next();
                            double sumToTake = Double.parseDouble(sumText);
                            mainAccount.withdrawSum(sumToTake);
                        }
                        break;
                        default: {
                            System.out.println("Your input to Main Account is wrong!");
                        }
                    }
                }
                if (accountToOperate.equals("Deposit")) {
                    switch (toDoString) {
                        case ("SHOW"): {
                            System.out.println("Current asset at Deposit Account: " + deposit.getAsset());
                        }
                        break;
                        case ("PUT"): {
                            System.out.println("Type sum to put");
                            String sumText = scanner.next();
                            double sumToPut = Double.parseDouble(sumText);
                            deposit.refillAccount(sumToPut);
                            long currentPutTime = System.currentTimeMillis();
                            deposit.setTimeOfLastPut(currentPutTime);
                        }
                        break;
                        case ("TAKE"): {
                            long lastPutTime = deposit.getTimeOfLastPut();
                            long currentWithdrawTime = System.currentTimeMillis();
                            long interval = currentWithdrawTime - lastPutTime;
                            Duration intervalInDays = Duration.ofMillis(interval);
                            if (intervalInDays.toDays() <= Deposit.getFixedNoTakeTime()) {
                                System.out.println("You may not withdraw money as the last put was within 30 days!");
                            } else {
                                System.out.println("Type sum to take");
                                String sumText = scanner.next();
                                double sumToTake = Double.parseDouble(sumText);
                                deposit.withdrawSum(sumToTake);
                            }
                        }
                        break;
                        default: {
                            System.out.println("Your input to Deposit is wrong!");
                        }
                    }
                }
                if (accountToOperate.equals("Card")) {
                    switch (toDoString) {
                        case ("SHOW"): {
                            System.out.println("Current asset at Card Account: " + cardAccount.getAsset());
                        }
                        break;
                        case ("PUT"): {
                            System.out.println("Type sum to put");
                            String sumText = scanner.next();
                            double sumToPut = Double.parseDouble(sumText);
                            cardAccount.refillAccount(sumToPut);
                        }
                        break;
                        case ("TAKE"): {
                            System.out.println("Type sum to take");
                            String sumText = scanner.next();
                            double sumToTake = Double.parseDouble(sumText);
                            if (sumToTake * CardAccount.getFINE() <= cardAccount.getAsset()) {
                                cardAccount.withdrawSum(sumToTake * CardAccount.getFINE());
                            } else {
                                System.out.println("Your asset is too small to exempt sum indicated and 1% fee");
                            }
                        }
                        break;
                        default: {
                            System.out.println("Your input to C is wrong!");
                        }
                    }
                }
            }
        }
    }
}
